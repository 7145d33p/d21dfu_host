#include <unistd.h>  //getopt
#include <stdio.h>
#include <string.h>
#include <libusb-1.0/libusb.h>

//=== bmRequest ===
#define	DFU_DETACH	0
#define DFU_DNLOAD	1
#define DFU_UPLOAD	2
#define DFU_GETSTATUS	3
#define DFU_CLRSTATUS	4
#define DFU_GETSTATE	5
#define DFU_ABORT	6

#define BUFFER_SIZE	128
#define MAX_FILE_SIZE	((256-4)*1024)
#define ROW_SIZE	256

#define FLASH_APP_START 0x1000

uint8_t data[BUFFER_SIZE];
uint8_t RxBuf[BUFFER_SIZE];
uint8_t FileBuf[MAX_FILE_SIZE];	//252KB
uint8_t RowBuf[ROW_SIZE];	//1 row = 4 page = 256 byte

void printdev(libusb_device *dev); //prototype of the function

void usbBulkWrite(libusb_device_handle *dh, unsigned char *pdata, int len)
{
	int r, actual;

	r = libusb_bulk_transfer(dh, (2 | LIBUSB_ENDPOINT_OUT), pdata, len, &actual, 0); //out EP2
	if ((r!=0) || (actual!=len)) printf("write error\n");
}

void usbBulkRead(libusb_device_handle *dh, unsigned char *pbuf, int maxlen)
{
	int r, actual, i;

	do {
		r = libusb_bulk_transfer(dh, (1 | LIBUSB_ENDPOINT_IN), RxBuf, maxlen, &actual, 100);
		//if (r==0) {
		//	for (i=0; i<actual; i++) printf("%02X ", RxBuf[i]);
		//	printf("\n");
		//}
	} while (r==0);
}

int ControlSend(libusb_device_handle *dh, uint8_t bRequest, uint16_t wValue, uint16_t wIndex, uint16_t wLength, uint8_t *buf)
{
	int res;
	uint8_t bmRequestType;

	switch (bRequest)
	{
		case (DFU_DNLOAD):
		case (DFU_CLRSTATUS):
			bmRequestType = 0x21; break;
		case (DFU_UPLOAD):
		case (DFU_GETSTATUS):
		default:
			bmRequestType = 0xa1; break;
	}

	res = libusb_control_transfer (dh, bmRequestType, bRequest, wValue, wIndex, buf, wLength, 1000);
	if (res<0) printf("SETUP packet error\n");
	return (res);
}

int DFUCmd_GetStatus(libusb_device_handle *dh)
{
	int r, i;

	r = ControlSend(dh, DFU_GETSTATUS, 0x0000, 0x0000, 0x0006, RxBuf);
	return (r);
}

int DFUCmd_Write1Row(libusb_device_handle *dh, int row, uint8_t *rbuf)
{
	int r;

	r=ControlSend(dh, DFU_DNLOAD, (uint16_t)row, 0x0000, 256, rbuf);     //send download command
        return r;
}

int DFUCmd_JMP0(libusb_device_handle *dh)
{
	int r;
	r = ControlSend(dh, DFU_DNLOAD, 0x0000, 0x0000, 0x0000, RxBuf);     //send JLMP command
        r = DFUCmd_GetStatus(dh); //it should timeout
}

int main(int argc, char *argv[]) 
{
	FILE *fp;	
	int fsz, i, addr, filesize;
	uint16_t rownum;
	int r, actual; 		//for return values and actual data sent
	ssize_t cnt; 		//holding number of devices in list
	int vid, pid;

	int iflag=0;
	int jflag=0;
	int pflag=0;
	int Iflag=0;
	char *cvalue = NULL;
	int index, c;

	while ((c=getopt(argc, argv, "hip:jI:")) != -1) {
		switch(c) {
			case 'h':
				printf("USAGE: d21dfu -hipjI [VID.PID] [binaryfile]\n");
				printf("   -h           : print this menu\n");
				printf("   -i           : print USB device info\n");
				printf("   -p binfile   : program binfile\n");
				printf("   -j           : jump to application code\n");
				printf("   -I VVVV.PPPP : use this VID and PID\n");
				return(0);
			case 'i':
				iflag = 1; break;
			case 'j':
				jflag = 1; break;
			case 'p':
				if ((fp = fopen(optarg, "rb")) == NULL) {
					printf("ERROR: fail to open %s\n", optarg);
					return(1);
				}
	
				fseek(fp, 0, SEEK_END);
				filesize = ftell(fp);
				rewind(fp);
				if (filesize > MAX_FILE_SIZE) {
					printf("ERROR: file too large\n");
					return(1);
				}
				fsz = fread(FileBuf, filesize, 1, fp);
				pflag = 1;
				break;
			case 'I':
				if (sscanf(optarg, "%04x.%04x", &vid, &pid) == 2) {
					printf ("VID: %04X PID: %04X\n", vid, pid);
					Iflag = 1;
				} else {
					printf ("VID/PID format error\n");
					return(0);
				}
				break;
			case '?':
			default:
				printf("options error\n");
				return(1);
		}
	}

	libusb_device **devs; //pointer to pointer of device, used to retrieve a list of devices
	libusb_context *ctx = NULL; //a libusb session
	libusb_device_handle *devh = NULL;

	r = libusb_init(&ctx); //initialize a library session
	if(r < 0) {
		printf("Init Error %d\n",r); //there was an error
		return 1;
	}
	libusb_set_debug(ctx, 3); //set verbosity level to 3, as suggested in the documentation

	cnt = libusb_get_device_list(ctx, &devs); //get the list of devices
	if(cnt < 0) {
		printf("Get Device Error\n"); //there was an error
	}

	//--- print device info options ---
	if (iflag) {
		printf("%d Devices in list\n", cnt); //print total number of usb devices
		for(i = 0; i < cnt; i++) { //iteration thru the list
			printdev(devs[i]); //print specs of this device
		}
	}

	//open a the DFU device
	devh = libusb_open_device_with_vid_pid(ctx, vid, pid); //0x03eb, 0x2ff4
	if (devh == NULL) { printf("\nopen device error. bye\n"); return (1); }
	//free device list, unref devices in it
	libusb_free_device_list(devs, 1);

	
	if (libusb_kernel_driver_active(devh, 0) == 1) {	//check if kernel driver is active
		printf ("Kernel driver is active\n");
		if (libusb_detach_kernel_driver(devh, 0) == 0) printf ("Kernel driver is detached\n");
	}

	r = libusb_claim_interface(devh, 0); //claim interface 0 (the first) of device
	if (r<0) { printf("cannot claim interface\n"); return(1); }

	//--- program option ------
	if (pflag) {
		r = DFUCmd_GetStatus(devh);
		for (addr=0, rownum=0; addr<filesize; addr+=ROW_SIZE, rownum++)  //1st byte of bin file => address 0x1000 of flash
		{	
			printf("Writing Row %d\n", rownum);
			memcpy(RowBuf, &FileBuf[addr], ROW_SIZE);
			DFUCmd_Write1Row(devh, rownum, RowBuf);
		}
	}
	//--- jump to apps option ---
	if (pflag || jflag) {
		r = DFUCmd_JMP0(devh);
	}

	printf("\n");
	r = libusb_release_interface(devh, 0); //release claimed interface
	if (r!=0) { printf("cannot release interface\n"); return(1); }
	printf("interface released\n");

	libusb_close(devh);	//close handle
	libusb_exit(ctx); //close the session
	return 0;
}

void printdev(libusb_device *dev) {
	struct libusb_device_descriptor desc;
	int r = libusb_get_device_descriptor(dev, &desc);
	if (r < 0) {
		printf("failed to get device descriptor\n");
		return;
	}
	printf("Number of possible configurations: %d\n",(int)desc.bNumConfigurations);
	printf("Device Class: %d\n",(int)desc.bDeviceClass);
	printf("VendorID    : %04X\n",desc.idVendor);
	printf("ProductID   : %04X\n",desc.idProduct);

	struct libusb_config_descriptor *config;
	libusb_get_config_descriptor(dev, 0, &config);
	printf("Interfaces: %d ||| \n",(int)config->bNumInterfaces);

	struct libusb_interface *inter;
	struct libusb_interface_descriptor *interdesc;
	struct libusb_endpoint_descriptor *epdesc;

	for(int i=0; i<(int)config->bNumInterfaces; i++) {
		inter = &config->interface[i];
		printf("Number of alternate settings: %d | ", inter->num_altsetting);
		for(int j=0; j<inter->num_altsetting; j++) {
			interdesc = &inter->altsetting[j];
			printf("Interface Number: %d | ",(int)interdesc->bInterfaceNumber);
			printf("Number of endpoints: %d | ",(int)interdesc->bNumEndpoints);
			for(int k=0; k<(int)interdesc->bNumEndpoints; k++) {
				epdesc = &interdesc->endpoint[k];
				printf("Descriptor Type: %d | ",(int)epdesc->bDescriptorType);
				printf("EP Address: %d | ",(int)epdesc->bEndpointAddress);
			}
		}
	}
	printf("\n\n\n");
	libusb_free_config_descriptor(config);
}

