### d21dfu ###

Linux code to program a bin file (1st byte is on address 0x1000) to D21 via a modified version of kevinmehall's DFU bootloader  
           USAGE: d21dfu -hipjI [VID.PID] [binaryfile]  
                         -h           : print this menu  
                         -i           : print USB device info  
                         -p binfile   : program binfile  
                         -j           : jump to application code  
                         -I VVVV.PPPP : use this VID and PID  
           # d21dfu -h  
           # d21dfu -I 03eb.2Ff4 -j  
           # d21dfu -I 03Eb.2fF4 -p blinkd21_0x1000.bin 

### To compile ###
 
gcc -std=gnu99 d21dfu.c -o d21dfu -lusb-1.0  


